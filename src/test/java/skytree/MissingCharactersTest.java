package skytree;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class MissingCharactersTest {

  @Before
  public void before() throws Exception {
  }

  @After
  public void after() throws Exception {
  }


  @Test
  public void testExample0() throws Exception {
//TODO: Test goes here...
    Main box = new skytree.Main();
    Assert.assertEquals("", box.getMissingLetters("A quick brown fox jumps over the lazy dog"));
    Assert.assertEquals("", box.getMissingLettersPreJava8("A quick brown fox jumps over the lazy dog"));

  }

  @Test
  public void testExample1() throws Exception {
//TODO: Test goes here...
    Main box = new skytree.Main();
    Assert.assertEquals("bjkmqz", box.getMissingLetters("A slow yellow fox crawls under the proactive dog"));
    Assert.assertEquals("bjkmqz", box.getMissingLettersPreJava8("A slow yellow fox crawls under the proactive dog"));

  }

  @Test
  public void testExample2() throws Exception {
//TODO: Test goes here...
    Main box = new skytree.Main();
    Assert.assertEquals("cfjkpquvwxz", box.getMissingLetters("Lions, and tigers, and bears, oh my!"));
    Assert.assertEquals("cfjkpquvwxz", box.getMissingLettersPreJava8("Lions, and tigers, and bears, oh my!"));

  }

  @Test
  public void testExample3() throws Exception {
//TODO: Test goes here...
    Main box = new skytree.Main();
    Assert.assertEquals("abcdefghijklmnopqrstuvwxyz", box.getMissingLetters(""));
    Assert.assertEquals("abcdefghijklmnopqrstuvwxyz", box.getMissingLettersPreJava8(""));
  }
} 
