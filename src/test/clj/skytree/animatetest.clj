(ns skytree.animatetest
  (:require [clojure.test :refer :all]
            [skytree.problem2 :refer :all]))

(deftest get-indexed-test
  (testing "testing blank get-indexed"
    (is (= (get-indexed \L "...") [])))
  (testing "testing blank get-indexed"
    (is (= (get-indexed \R "...") [])))  
  (testing "testing one L get-indexed"
    (is (= (get-indexed \L ".L.") [[1 \L]])))
  (testing "testing one R get-indexed"
    (is (= (get-indexed \R "..R") [[2 \R]])))  
  (testing "testing all R get-indexed"
    (is (= (get-indexed \R "RRRR") [[0 \R] [1 \R] [2 \R] [3 \R]]))))

(deftest get-to-string-test
  (testing "testing simple to-string"
    (is (= (to-string  [0 1 2 3] 4) "XXXX")))
  (testing "testing simple to-string"
    (is (= (to-string  [1 2 3] 9) ".XXX....."))))

(deftest animate-test
  (testing "example 0"
    (is (= (animate 2 "..R....")
           ["..X...."
            "....X.."
            "......X"
            "......."])))
  (testing "example 1"
    (is (= (animate 3 "RR..LRL")
           '("XX..XXX"
             ".X.XX.."
             "X.....X"
             "......."))))
  (testing "example 2"
    (is (= (animate 2 "LRLR.LRLR")
           '("XXXX.XXXX" "X..X.X..X" ".X.X.X.X." ".X.....X." "........."))))
  (testing "example 3"
    (is (= (animate 10 "RLRLRLRLRL")
           '("XXXXXXXXXX" ".........."))))
  (testing "example 4"
    (is (= (animate 1 "...") '("..."))))
  (testing "example 5"
    (is (= (animate 1  "LRRL.LR.LRR.R.LRRL.")
           '("XXXX.XX.XXX.X.XXXX." "..XXX..X..XX.X..XX." ".X.XX.X.X..XX.XX.XX" "X.X.XX...X.XXXXX..X" ".X..XXX...X..XX.X.." "X..X..XX.X.XX.XX.X." "..X....XX..XX..XX.X" ".X.....XXXX..X..XX." "X.....X..XX...X..XX" ".....X..X.XX...X..X" "....X..X...XX...X.." "...X..X.....XX...X." "..X..X.......XX...X" ".X..X.........XX..." "X..X...........XX.." "..X.............XX." ".X...............XX" "X.................X" "...................")))))
  
  
