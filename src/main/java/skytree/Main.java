package skytree;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
  private final static List<Character> ALL_LETTERS = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');

  private boolean isExists(Character c, String sent) {
    Character upCase = Character.toUpperCase(c);
    for (int i = 0; i < sent.length(); i++) {
      if (c.equals(sent.charAt(i))) return true;
      if (upCase.equals(sent.charAt(i))) return true;
    }
    return false;
  }

  // way to do in java 7 or prior
  public String getMissingLettersPreJava8(String sent) {
    StringBuffer ret = new StringBuffer();
    for (Character c : ALL_LETTERS) {
      if (!isExists(c, sent)) {
        ret.append(c);
      }
    }
    return ret.toString();
  }

  // requires Java 8 lambdas and streams
  public String getMissingLetters(String sent) {
    Stream<?> result = ALL_LETTERS.stream().map(c -> !isExists(c, sent) ? c : "" );
    return result.map(Object::toString).collect(Collectors.joining(""));
  }

  public static void main(String[] args) {
    String sentence = args[0];
    Main m = new Main();
    String missing = m.getMissingLetters(sentence);
    System.out.println("Sentence: " + sentence);
    System.out.println("missing: " + missing);
  }
}
