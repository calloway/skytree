(ns skytree.main
  (require [clojure.string :refer [lower-case]]))

(def letters (map char (range 97 123)))  ;; a-z

(defn findmissing [sent]
  (let [tolower (lower-case sent)
        exists? #(not (nil? (some #{%1} %2)))]
    (apply str (map #(if (exists? % tolower) ""  %) letters))))

(defn -main
  "I don't do a whole lot."
  [& args]
  (println (findmissing (first args))))
