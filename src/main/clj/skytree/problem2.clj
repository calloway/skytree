(ns skytree.problem2)

(defn get-indexed [ch input]
  (filter (fn[[k v]] (and (< k (count input)) (= v ch))) 
          (map-indexed (fn [idx itm] [idx itm]) input)))

(defn to-string [particles size]
  (loop [ls particles acc (into [] (repeat size "."))]
    (if (empty? ls)
      (apply str acc)
      (recur (rest ls) (assoc acc (first ls) "X")))))

(defn move [particles speed size]
  (filter (fn [[pos _]] (and (>= pos 0) (< pos size))) (map (fn [[pos dir]]
                                                               [((if (= dir \L) - +) pos speed) dir]) particles)))

(defn animate [speed input]
  (loop [rightparticles (get-indexed \R input)
         leftparticles (get-indexed \L input)
         acc '()]
    (let [size (count input)
          positions (fn [particles] (map #(first %) particles))
          particles (distinct (concat (positions leftparticles) (positions rightparticles)))
          chamber (to-string particles (count input))]
      (if (empty? particles)
        (reverse (cons chamber acc))
        (recur (move rightparticles speed size) (move leftparticles speed size) (cons chamber acc))))))
